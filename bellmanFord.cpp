#include<bits/stdc++.h>
#define N 5

using namespace std;

int graph[N][N];
int prev[N],d[N],visited[N];
int u,v;

void inputGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        for(j=0; j<N; j++)
        {
            cin>>graph[i][j];
        }
    }
}

void InputedGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        cout<<endl;
        for(j=0; j<N; j++)
        {
            cout<<graph[i][j]<<" ";
        }
    }
    cout<<endl;
}


int bellman_ford()
{
    int u,v,i;
    for(u=0;u<N;u++)
    {
        d[u] = 1000;
    }
    d[0] = 0;
    for(i=0;i<N-1;i++)
    {
        for(u=0;u<N;u++)
        {
            for(v=0;v<N;v++)
            {
                if(graph[u][v] != 0)
                {
                    if(d[v] > d[u] + graph[u][v])
                    {
                        d[v] = d[u] + graph[u][v];
                    }
                }
            }
        }
    }
     for(u=0;u<N;u++)
     {
            for(v=0;v<N;v++)
            {
                if(graph[u][v] != 0)
                {
                    if(d[v] > d[u] + graph[u][v])
                    {
                        return 0;
                    }
                }
            }
        }
        return 1;


}


void outputGraph()
{
    int u;
    for(u=0;u<N;u++)
    {
        //printf(" Node %d : shortest distance %d\n", u,d[u]);
        cout<<"Node 0 to "<<u<<" : shortest distance "<<d[u]<<endl;
    }
}


int main()
{
    cout<<"Input adj matrix of graph"<<endl;
    inputGraph();
    cout<<endl<<"Inputed graph print"<<endl;
    InputedGraph();
    cout<<endl;
    if(bellman_ford() == 1)
        outputGraph();
    else
        cout<<"there exist atleast one negative cycle"<<endl;

}
