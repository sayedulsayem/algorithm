#include<bits/stdc++.h>
#define N 5

using namespace std;

int graph[N][N];

void inputGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        for(j=0; j<N; j++)
        {
            cin>>graph[i][j];
        }
    }
}

void InputedGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        cout<<endl;
        for(j=0; j<N; j++)
        {
            cout<<graph[i][j]<<" ";
        }
    }
    cout<<endl;
}

void floyedWarshall()
{
    for(int k=0; k<N; k++)
    {
        for(int i=0; i<N; i++)
        {
            for(int j=0; j<N; j++)
            {
                if(graph[i][k]+graph[k][j]<graph[i][j])
                {
                    graph[i][j]=graph[i][k]+graph[k][j];
                }
            }
        }
    }
}


void outputGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        cout<<endl;
        for(j=0; j<N; j++)
        {
            cout<<graph[i][j]<<" ";
        }
    }
    cout<<endl;
}


int main()
{
    cout<<"Input adj matrix of graph"<<endl;
    inputGraph();
    cout<<endl<<"Inputed graph print"<<endl;
    InputedGraph();
    cout<<endl;
    floyedWarshall();
    cout<<"Output graph"<<endl;
    outputGraph();

}
