#include<bits/stdc++.h>
#define N 5

using namespace std;

int graph[N][N];
int prev[N],d[N],visited[N];
int u,v;

void inputGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        for(j=0; j<N; j++)
        {
            cin>>graph[i][j];
        }
    }
}

void InputedGraph()
{
    int i,j;
    for(i=0; i<N; i++)
    {
        cout<<endl;
        for(j=0; j<N; j++)
        {
            cout<<graph[i][j]<<" ";
        }
    }
    cout<<endl;
}
int extractMin()
{
    int minValue=100;
    int min = -1;
    for(u=0; u<=N; u++)
    {
        if(d[u]<=minValue && visited[u]==0)
        {
            minValue=d[u];
            min=u;
        }
    }
    return min;
}
void dijstra()
{
    for(u=0; u<N; u++)
    {
        if(u==0)
        {
            d[u]=0;
        }
        else
        {
           d[u]= 100000;
        }
        visited[u]=0;
    }
    u=extractMin();
    while(u!=-1)
    {
        visited[u]=1;
        for(v=0; v<N; v++)
        {
            if(graph[u][v]>0)
            {
                if(d[v]>d[u]+graph[u][v])
                {
                    d[v]=d[u]+graph[u][v];
                    prev[v]=u;
                }

            }
        }
        u=extractMin();
    }
}

void path(int x,int y){
    int c=0;
    cout<<"path <-- "<<y<<endl;
    if(y==x){
        return;
    }
    else
    {
      path(x,prev[y]);
    }
}

void outputResult()
{
    int u;
    for(u=0; u<N; u++)
    {
        cout<<"Node 0 to "<<u<<"  sortest distance "<<d[u]<<endl;
        path(0,u);
    }
}


int main()
{
    cout<<"Input adj matrix of graph"<<endl;
    inputGraph();
    cout<<endl<<"Inputed graph print"<<endl;
    InputedGraph();
    cout<<endl;
    dijstra();
    outputResult();


}
