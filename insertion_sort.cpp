#include<bits/stdc++.h>
using namespace std;
int a[10000000];
void input(int x){
    cout<<"Enter array elements"<<endl;
    for(int i=0;i<x;i++){
        a[i]=rand()%100;
    }
}
void output(int x){
    for(int j=0;j<x;j++){
        cout<<a[j]<<" ";
    }
    cout<<endl;
}
int main(){
    int n,i,j,p1,p2,tmp;
    //cout<<"Enter the size of array"<<endl;
    //cin>>n;
    n=1000000;
    input(n);
    //cout<<"Inserted array"<<endl;
    //output(n);
    int first_part=n/2;
    clock_t t;
    t=clock();
    for(i=1;i<first_part;i++){
        tmp=a[i];
        p2=i;
        while(p2>0&&a[p2-1]>tmp){
            a[p2]=a[p2-1];
            p2--;
        }
        a[p2]=tmp;
    }
    for(i=first_part+1;i<n;i++){
        tmp=a[i];
        p2=i;
        while(p2>0&&a[p2-1]<tmp){
            a[p2]=a[p2-1];
            p2--;
        }
        a[p2]=tmp;
    }
    t=clock()-t;
    double t_t=(double)t/CLOCKS_PER_SEC;
    cout<<"execution time : "<<t_t<<endl;
    //cout<<"Sorted array"<<endl;
    //output(n);
}
