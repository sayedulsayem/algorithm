#include<bits/stdc++.h>
#define inf 10000
using namespace std;
int a[100][100];

void rockClimbing()
{
    for(int i=0; i<=6; i++)
    {
        a[i][0]=inf;
        a[i][6]=inf;
    }
    cout<<"input a graph"<<endl;
    for(int i=0; i<5; i++)
    {
        for(int j=1; j<=5; j++)
            cin>>a[i][j];
    }
    for(int i=1; i<5; i++)
    {
        for(int j=1; j<=5; j++)
        {
            a[i][j] = a[i][j] + (min(a[i-1][j-1],min(a[i-1][j],(a[i-1][j+1]))));
        }
    }
}

void outputGraph()
{
    cout<<"output the climbing graph"<<endl;
    for(int i=0; i<5; i++)
    {
        for(int j=0; j<=6; j++)
        {
            cout<<a[i][j]<<" ";
        }
        cout<<endl;
    }
}

void minimumDistance()
{
    int mi=inf;
    for(int j=1; j<=5; j++)
    {
        mi=min(mi,a[4][j]);
    }
    cout<<"min "<<mi<<endl;
}

int main()
{
    rockClimbing();
    outputGraph();
    minimumDistance();
}
