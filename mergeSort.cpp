#include<bits/stdc++.h>

using namespace std;

int a[100000];

void buildArray(int n)
{
    cout<<"Enter array element"<<endl;
    for ( int i= 0 ; i< n; i++)
    {
        cin>>a[i];
    }
}

void printArray(int n)
{
    for ( int i= 0 ; i< n ; i++)
    {
        cout<<a[i]<<" ";
    }
    cout<<endl;
}

void merge(int left, int center, int right)
{
    int k;
    int n1=center-left +1;
    cout<<"the value of n1 : "<<n1<<endl;
    int n2=right-center;
    cout<<"the value of n2 : "<<n2<<endl;
    int l[n1],r[n2];
    for(int i=0; i<n1; i++)
    {
        l[i]=a[left+i];
    }
    for(int j=0; j<n2; j++)
    {
        r[j]=a[center+j+1];
    }
    int i=0;
    int j=0;
    for (k=left; k<=right; k++)
    {
        if(i<n1&&j<n2)
        {
            if (l[i]<=r[j])
            {
                a[k]=l[i];
                i++;
            }
            else
            {
                a[k]=r[j];
                j++;
            }
        }
        else
        {
            break;
        }

    }
    while(i<n1)
    {
        a[k]=l[i];
        i++;
        k++;
    }
    while(j<n2)
    {
        a[k]=r[j];
        j++;
        k++;
    }
}

void mergeSort(int left, int right)
{
    if(left<right)
    {
        int center= (left+right)/2;
        cout<<"called left mergeSort function with "<<left<<" "<<center<<endl;
        mergeSort(left,center);
        cout<<"called right mergeSort function with "<<center+1<<" "<<right<<endl;
        mergeSort(center+1,right);
        cout<<"called merge function with "<<" "<<left<<" "<<center<<" "<<right<<endl;
        merge(left,center,right);
    }

}

int main()
{
    int n;
    cout<<"Enter array size"<<endl;
    cin>>n;
    buildArray(n);
    cout<<"Array print"<<endl;
    printArray(n);
    mergeSort(0,n-1);
    cout<<"Print sorted array"<<endl;
    printArray(n);

}
