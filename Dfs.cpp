#include<bits/stdc++.h>
#define N 4
#define White 0
#define Gray 1
#define Black 2

using namespace std;

int tSort[10];
int tIndex=0;
int graph[N][N];
int color[N],prev[N],d[N],f[N],t=0;

void inputGraph(){
    int i,j;
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            cin>>graph[i][j];
        }
    }
}

void printGraph(){
    int i,j;
    for(i=0;i<N;i++){
        cout<<endl;
        for(j=0;j<N;j++){
            cout<<graph[i][j]<<" ";
        }
    }
}

void dfsVisit(int u){

    int v;
    color[u]=Gray;
    t++;
    d[u]= t;
    for(v=0;v<N;v++){
        if(graph[u][v]==1 && color[v]==White){
            prev[v]=u;
            dfsVisit(v);
        }
    }

    color[u]=Black;
    t++;
    f[u]=t;
    tSort[tIndex]=u;
    tIndex++;

}

void dfs(){
    int u;
    for(u=0;u<N;u++){
        color[u]=White;
        prev[u]=-1;
        d[u]= 1000;
        f[u]= 1000;
    }
    t = 0;
    for(u=0;u<N;u++){
        if (color[u]==White){
            dfsVisit(u);
        }
    }

}

void outputResult(){
    int u;
    for(u=0;u<N;u++){
        cout<<"Node "<<u<<" : start "<<d[u]<<" : end "<<f[u]<<endl;
    }
}

void tropologycalSort(){
    int n=tIndex-1;
    for(int i=n;i>=0;i--){
        cout<<tSort[i]<<" ";
    }
}

void path(int x,int y){
    int c=0;
    if (c==0)
    {
       cout<<y<<" ";
    }
    cout<<prev[y]<<" ";
    if(prev[y]==x){
        return;
    }
    else
    {
      path(x,prev[y]);
    }
}

int main(){
    cout<<"Input adj matrix of graph"<<endl;
    inputGraph();
    cout<<endl<<"Inputed graph print"<<endl;
    printGraph();
    cout<<endl;

    dfs();
    cout<<endl<<"Result after done dfs"<<endl<<endl;
    outputResult();

    cout<<endl<<"Tropologycal sort"<<endl;
    tropologycalSort();

    cout<<endl<<"path between two node"<<endl<<endl;
    path(0,3);

}
